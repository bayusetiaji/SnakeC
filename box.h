/*
 * interface fungsi-fungsi bantu
*/

#pragma once

#include <string>

void goto_xy(const int, const int);
void text_color(const int);
void draw_box(const int, const int, const int, const int);